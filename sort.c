#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <float.h>
#include <math.h>

long long int swaps_counter;
long long int compares_counter;

const double ABS_DBL_MIN = 1;
const double ABS_DBL_MAX = 10;

void
gen_array(double *array, size_t array_len, unsigned char type);

void
selection_sort(double *array, size_t array_len);

void
heapify(double *array, size_t array_len, size_t i);

void
heapsort(double *array, size_t array_len);

int
test_sort(double *prim_array, double *test_array, size_t array_len);

double
frand(double low, double high);

void
swap(double *a, double *b);

int
comp(const void *elem1, const void *elem2);

int
double_equals(const double a, const double b);

int 
main(void) 
{
    double *prim_array;
    double *select_array;
    double *heap_array;
    unsigned char case_number;

    srand( time(NULL) );
    case_number = 1;
    
    /** 
     * There are 4 types of arrays.
     * 1 - Elements are sorted
     * 2 - Elements are sorted in back order
     * >= 3 - Elements are sorted in random order
     */
    for (unsigned char type = 1; type <= 4; type++) {
        for (size_t array_len = 10; array_len <= 10000; array_len *= 10) {
            printf("Case #%u\nType of start elements state: %u\nArray length is %zu\n",
                    case_number, type, array_len);
            
            prim_array = calloc( array_len, sizeof(double) );
            select_array = calloc( array_len, sizeof(double) );
            heap_array = calloc( array_len, sizeof(double) );

            gen_array(prim_array, array_len, type);
            
            memcpy(select_array, prim_array, array_len * sizeof(double));
            memcpy(heap_array, prim_array, array_len * sizeof(double));
            qsort(prim_array, array_len, sizeof(double), comp);

            swaps_counter = 0;
            compares_counter = 0;
            selection_sort(select_array, array_len);
            
            if ( test_sort(prim_array, select_array, array_len) < 0 ) {
                printf("Some error has occurred while array has been sorting.\n");

                free(prim_array);
                free(select_array);
                free(heap_array);

                return -1;
            }

            printf("==> Selection sort data. Number of swaps is %lld and number of compares is %lld\n", 
                    swaps_counter, compares_counter);

            swaps_counter = 0;
            compares_counter = 0;
            heapsort(heap_array, array_len);
            
            if ( test_sort(prim_array, heap_array, array_len) < 0 ) {
                printf("Some error has occurred while array has been sorting.\n");

                free(prim_array);
                free(select_array);
                free(heap_array);

                return -1;
            }

            printf("==> Heapsort data. Number of swaps is %lld and number of compares is %lld\n\n", 
                swaps_counter, compares_counter);
            
            free(prim_array);
            free(select_array);
            free(heap_array);

            case_number++;
        }
    }

    return 0;
}

void
gen_array(double *array, size_t array_len, unsigned char type)
{
    array[0] = frand(ABS_DBL_MIN, ABS_DBL_MAX);    
    
    for (size_t i = 1; i < array_len; i++) {
        if (type == 1) {
            array[i] = frand( ABS_DBL_MIN, fabs(array[i - 1]) );
        } else if (type == 2) {
            array[i] = frand(fabs(array[i - 1]), ABS_DBL_MAX);
        } else {
            array[i] = frand(ABS_DBL_MIN, ABS_DBL_MAX);
        }
    }
}

void 
selection_sort(double *array, size_t array_len)
{
    size_t max_idx;

    for (size_t i = 0; i < array_len - 1; i++) {
        max_idx = i;

        for (size_t j = i + 1; j < array_len; j++) {
            if ( ++compares_counter && ( fabs(array[j]) > fabs(array[max_idx]) ) ) {
                max_idx = j;
            }
        }

        if (max_idx != i) {
            swaps_counter++;
        }

        swap(&array[max_idx], &array[i]);
    }
}

void 
heapify(double *array, size_t array_len, size_t i) {
    size_t min = i;
    size_t left_child = 2 * i + 1;
    size_t right_child = 2 * i + 2;
  
    if ( ++compares_counter && ( left_child < array_len && fabs(array[left_child]) < fabs(array[min] ) ) ) {
      min = left_child;
	}
  
    if ( ++compares_counter && ( right_child < array_len && fabs(array[right_child]) < fabs(array[min] ) ) ) {
      min = right_child;
    }
  
    if (min != i) {
        swaps_counter++;

        swap(&array[i], &array[min]);
        heapify(array, array_len, min);
    }
}

void 
heapsort(double *array, size_t array_len)
{
    for (int i = array_len / 2 - 1; i >= 0; i--) {
        heapify(array, array_len, i);
    }
  
    for (int i = array_len - 1; i >= 0; i--) {
        swaps_counter++;

        swap(&array[0], &array[i]);
        heapify(array, i, 0);
    }
}

int
test_sort(double *prim_array, double *test_array, size_t array_len)
{
    for (size_t i = 1; i < array_len; i++) {
        if ( !double_equals( fabs(prim_array[i]), fabs(test_array[i]) ) ) {
            return -1;
        }
    }

    return 0;
}

double
frand(double low, double high)
{
    double d;

    d = ( (double)rand() * ( high - low ) ) / (double)RAND_MAX + low;

    if (rand() % 2) {
        d = -d;
    }

    return d;
}

void
swap(double *a, double *b)
{
    double temp = *b;
    *b = *a;
    *a = temp;
}

int
comp(const void *elem1, const void *elem2)
{
    double a = *((double *)elem1);
    double b = *((double *)elem2);

    if ( fabs(a) > fabs(b) ) {
        return -1;
    } else if ( fabs(a) < fabs(b) ) {
        return 1;
    } else {
        return 0;
    }
}

int
double_equals(const double a, const double b)
{
    return ( fabs(a - b) < ( DBL_EPSILON * fabs(a + b) ) );
}
